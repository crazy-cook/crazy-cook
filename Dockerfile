#FROM gcr.io/google-appengine/nodejs

FROM node:12-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm install

RUN npm run build

EXPOSE 8080 8088 8888

CMD ["npm", "run", "start-pro"]