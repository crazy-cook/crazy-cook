const router = require('../bin/routes.js')
const { gamesMulti } = require('./startGameApi')

router.route('/apis/gameReady')
    .post((req, res) => {
        if (typeof req.query.id === 'undefined') return res.sendStatus(400);
        let game = gamesMulti.get(req.query.id)
        try {
            if (typeof game.playerId2 !== 'undefined' && game.playerId2 !== null){
                res.sendStatus(201)
            } else {
                res.sendStatus(301)
            }
        } catch (e) {
            res.sendStatus(302)
        }
    })

/**
 * @swagger
 * /apis/gameReady:
 *      post:
 *          summary: Verifica se um Jogo Multiplayer já tem os 2 jogadores
 *          tags:
 *            - Game
 *          description: Verifica se um Jogo Multiplayer já tem os 2 jogadores
 *          parameters:
 *            - in: query
 *              name: id
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              300:
 *                  description: Not ready
 *              200:
 *                  description: Ready
 */