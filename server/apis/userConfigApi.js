const router = require('../bin/routes.js')
const configDB = require('../accessDB/configDB')
const config = require('../models/config')
const { configs } = require('../functions/openGame')

router.route('/apis/config')
    .post((req, res) => {
        if (req.body) {
            let conf = Object.create(config);
            conf.fromJson(req.body);
            configs.set(conf.playerid, conf)
            configDB.putOne(conf)
                .then(r => {
                    res.sendStatus(200)
                })
                .catch(err => {
                    res.sendStatus(500)
                })
        } else {
            res.sendStatus(400)
        }
    })

/**
 * @swagger
 * /apis/config:
 *      post:
 *          summary: Recebe uma config
 *          tags:
 *            - Player
 *          description: Recebe uma config
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              playerid:
 *                                  type: string
 *                                  example: 1
 *                              theme:
 *                                  type: integer
 *                                  example: 1
 *          responses:
 *              200:
 *                  description: OK
 *                  content:
 *                      text/plain
 */