const router = require('../bin/routes.js');
const openGame = require('../functions/openGame');

router.route('/apis/openGame')
    .get((req, res) => {
        let id = req.query.sessionId;
        let session = openGame.sessions.get(id);
        let config = openGame.configs.get(id);
        if (typeof session === 'undefined' || typeof config === 'undefined') {
            res.sendStatus(500);
            openGame.getFromDB(id);
        } else {
            res.status(200).send({
                    player: session.toJson(),
                    config: config.toJson()
                })
        }
    });

/**
 * @swagger
 * /apis/openGame:
 *      get:
 *          summary: Retorna os dados do player
 *          tags:
 *            - Game
 *          description: Retorna os dados do player e as suas configurações na abertura do jogo. Pode retornar erro (500) caso a sessão não esteja em memoria (ira buscar da DB), tentar novamente apos algum tempo
 *          parameters:
 *            - in: query
 *              name: sessionId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: OK
 */