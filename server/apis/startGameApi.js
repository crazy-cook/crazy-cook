const router = require('../bin/routes.js')
const { createGame } = require("../functions/play");
const { gameMode } = require('../functions/enums');

let gamesMulti = new Map();

router.route('/apis/startGame')
    // cria e devolve um novo jogo, com o playerId, modo (single/multi) e dificuldade
    .get((req, res) => {
        if (req.query.playerId && req.query.mode && req.query.difficulty) {
            createGame(req.query.playerId, parseInt(req.query.mode), req.query.difficulty)
                .then(game => {
                    res.status(200).send(game.toJson())
                    if (parseInt(req.query.mode) === gameMode.MULTIPLAYER) {
                        gamesMulti.set(game.id, game)
                    }
                }).catch(err => {
                    res.sendStatus(500)
                })
        } else {
            res.sendStatus(400)
        }
    })

module.exports = { gamesMulti }

/**
 * @swagger
 * /apis/startGame:
 *      get:
 *          summary: Inicializa um novo jogo
 *          tags:
 *            - Game
 *          description: Inicializa um novo jogo
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *            - in: query
 *              name: mode
 *              required: true
 *              schema:
 *                  type: integer
 *              example: 0
 *            - in: query
 *              name: difficulty
 *              required: true
 *              schema:
 *                  type: string
 *              example: EASY
 *          responses:
 *              200:
 *                  description: OK
 *                  content:
 *                      application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: string
 *                                  example: f2c98334-ea2d-4cf2-9862-51d34a8220d0
 *                              difficulty:
 *                                  type: string
 *                                  example: EASY
 *                              playerId1:
 *                                  type: string
 *                                  example: 1
 *                              playerId2:
 *                                  type: string
 *                                  example: null
 *                              status:
 *                                  type: integer
 *                                  example: 1
 *                              mode:
 *                                  type: integer
 *                                  example: 0
 *                              cookieCoins:
 *                                  type: integer
 *                                  example: 0
 *                              ingredients:
 *                                  type: array
 *                                  example: ["Agua","Beterraba","Boi","Cebola","Couve","Galinha","Hortelã","Ovo","Rabo","Sopa"]
 *                              time:
 *                                  type: integer
 *                                  example: 7
 *                              recipes:
 *                                  type: array
 *                                  example: [{"name":"Sopa de Rabo de Boi","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Boi","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]},{"name":"Sopa de Rabo de Galinha","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Galinha","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]}]
 */