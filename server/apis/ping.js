const router = require('../bin/routes.js')

router.route('/apis/ping')
    .get((req, res) => {
        res.status(215).send('pong')
    })
    .post((req, res) => {
        res.status(216).send('pong')
    })
    .put((req, res) => {
        res.status(217).send('pong')
    })
    .delete((req, res) => {
        res.status(218).send('pong')
    })

/**
 * @swagger
 * /apis/ping:
 *      get:
 *          summary: Retorna 'pong'
 *          tags:
 *            - Ping
 *          description: Retorna 'pong' e o código 215
 *          responses:
 *              215:
 *                  description: pong
 *                  content:
 *                      text/html
 *      post:
 *          summary: Retorna 'pong'
 *          tags:
 *            - Ping
 *          description: Retorna 'pong' e o código 216
 *          responses:
 *              216:
 *                  description: pong
 *                  content:
 *                      text/html
 *      put:
 *          summary: Retorna 'pong'
 *          tags:
 *            - Ping
 *          description: Retorna 'pong' e o código 217
 *          responses:
 *              217:
 *                  description: pong
 *                  content:
 *                      text/html
 *      delete:
 *          summary: Retorna 'pong'
 *          tags:
 *            - Ping
 *          description: Retorna 'pong' e o código 218
 *          responses:
 *              218:
 *                  description: pong
 *                  content:
 *                      text/html
 */