const router = require('../bin/routes.js')
const playerDB = require('../accessDB/playerDB')

router.route('/apis/player')
    .get((req, res) => {
        if (req.query.playerId) {
            playerDB.getOne(req.query.playerId)
                .then(r => {
                    res.status(200).send(r)
                })
                .catch(err => {
                    res.sendStatus(500)
                })
        } else {
            res.sendStatus(400)
        }
    })

/**
 * @swagger
 * /apis/player:
 *      get:
 *          summary: Retorna os dados do player
 *          tags:
 *            - Player
 *          description: Retorna os dados do player
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: OK
 *                  content:
 *                      application/json:
 */