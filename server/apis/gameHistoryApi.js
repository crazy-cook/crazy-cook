const router = require('../bin/routes.js')
const gameHistoryDB = require('../accessDB/gameHistoryDB')

router.route('/apis/gameHistory')
    .get((req, res) => {
        if (req.query.playerId) {

            gameHistoryDB.getAllByPlayerId(req.query.playerId).then(r => {
                res.status(200).send(r);
            }).catch(e => {
                res.sendStatus(500);
            });

        } else {
            res.sendStatus(400);
        }
    });

/**
 * @swagger
 * /apis/gameHistory:
 *      get:
 *          summary: Retorna o histórico de jogos do player
 *          tags:
 *            - Statistics
 *          description: Retorna o histórico de jogos do player
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: OK
 *                  content:
 *                      application/json:
 */