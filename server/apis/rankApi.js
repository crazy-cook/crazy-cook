const router = require('../bin/routes')
const rank = require('../functions/rank')

router.route('/apis/ranks')
    .get((req, res) => {
        if (req.query.playerId) {

            rank.getRank(req.query.playerId).then(r => {
                res.status(200).send(r);
            }).catch(e => {
                res.sendStatus(500);
            });

        } else {
            res.sendStatus(400);
        }
    });

/**
 * @swagger
 * /apis/ranks:
 *      get:
 *          summary: Retorna o Top 5 do rank e a posição e estatisticas do player
 *          tags:
 *            - Statistics
 *          description: Retorna o Top 5 do rank e a posição e estatisticas do player
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: OK
 *                  content:
 *                      application/json:
 */