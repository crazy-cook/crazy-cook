const router = require('../bin/routes.js')
const { gamesMulti } = require('./startGameApi')

router.route('/apis/game')
    .get((req, res) => {
        if (typeof req.query.id === 'undefined') return res.sendStatus(400);
        let game = gamesMulti.get(req.query.id)
        try {
            if (typeof game.playerId2 !== 'undefined' && game.playerId2 !== null){
                res.status(200).send(game.toJson())
            } else {
                res.sendStatus(300)
            }
        } catch (e) {
            res.sendStatus(300)
        }
    })

/**
 * @swagger
 * /apis/game:
 *      get:
 *          summary: Verifica se um Jogo Multiplayer já tem os 2 jogadores, se tiver devolve o Jogo
 *          tags:
 *            - Game
 *          description: Verifica se um Jogo Multiplayer já tem os 2 jogadores, se tiver devolve o Jogo
 *          parameters:
 *            - in: query
 *              name: id
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              300:
 *                  description: Not ready
 *              200:
 *                  description: Ready
 *                  content:
 *                      application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: string
 *                                  example: f2c98334-ea2d-4cf2-9862-51d34a8220d0
 *                              difficulty:
 *                                  type: string
 *                                  example: EASY
 *                              playerId1:
 *                                  type: string
 *                                  example: 1
 *                              playerId2:
 *                                  type: string
 *                                  example: 2
 *                              status:
 *                                  type: integer
 *                                  example: 1
 *                              mode:
 *                                  type: integer
 *                                  example: 0
 *                              cookieCoins:
 *                                  type: integer
 *                                  example: 0
 *                              ingredients:
 *                                  type: array
 *                                  example: ["Agua","Beterraba","Boi","Cebola","Couve","Galinha","Hortelã","Ovo","Rabo","Sopa"]
 *                              time:
 *                                  type: integer
 *                                  example: 7
 *                              recipes:
 *                                  type: array
 *                                  example: [{"name":"Sopa de Rabo de Boi","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Boi","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]},{"name":"Sopa de Rabo de Galinha","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Galinha","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]}]
 */