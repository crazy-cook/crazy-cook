const router = require('../bin/routes.js')
const gameClass = require("../models/game.js")
const { gameStatus } = require("../functions/enums")

router.route('/apis/cancelGame')
    .get((req, res) => {
        if (typeof req.query.id === 'undefined') return res.sendStatus(400);
        try {
            let game1 = Object.create(gameClass);
            game1.getFromDB(req.query.id).then(game => {
                game.setStatus(gameStatus.ENDED)
                game.save()
            })
            res.sendStatus(200)
        } catch (e) {
            res.sendStatus(500)
        }
    })

/**
 * @swagger
 * /apis/cancelGame:
 *      get:
 *          summary: Marca um Jogo Multiplayer como terminado
 *          tags:
 *            - Game
 *          description: Marca um Jogo Multiplayer como terminado
 *          parameters:
 *            - in: query
 *              name: id
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: Ok
 */