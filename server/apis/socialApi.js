const router = require('../bin/routes')
const { sendPublish } = require('../accessCore/ws')

router.route('/apis/social')
    .get((req, res) => {
        if (req.query.playerId) {
            sendPublish(req.query.playerId, req.query.message).then(r => {
                res.sendStatus(200);
            }).catch(e => {
                res.sendStatus(500);
            });
        } else {
            res.sendStatus(400);
        }
    });

/**
 * @swagger
 * /apis/social:
 *      get:
 *          summary: Publica nas Redes Sociais
 *          tags:
 *            - Outro
 *          description: Publica nas Redes Sociais
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *            - in: query
 *              name: message
 *              required: true
 *              schema:
 *                  type: string
 *              example: Estou a jogar este jogo! Tenta fazer melhor!
 *          responses:
 *              200:
 *                  description: OK
 */