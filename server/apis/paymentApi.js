const router = require('../bin/routes.js');
const payment = require('../functions/payment');

router.route('/apis/payment')
    .get((req, res) => {

        if (req.query.playerId && req.query.value) {

            payment.requestPayment(req.query.playerId, "Crazy Cook Booster", req.query.value, res)
                .then(r => {})
                .catch(e => {
                    res.sendStatus(500);
                });

        } else {
            res.sendStatus(400);
        }
    });

/**
 * @swagger
 * /apis/payment:
 *      get:
 *          summary: Pede um pagamento
 *          tags:
 *            - Game
 *          parameters:
 *            - in: query
 *              name: playerId
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *            - in: query
 *              name: value
 *              required: true
 *              schema:
 *                  type: string
 *              example: 1
 *          responses:
 *              200:
 *                  description: OK
 */