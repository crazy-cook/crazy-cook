const router = require('../bin/routes.js')
const {endGame} = require("../functions/play");

router.route('/apis/endGame')
    .post((req, res) => {
        console.log(req.body)
        endGame(req.body)
            .then(r => res.sendStatus(200))
            .catch(err => res.sendStatus(500))
    })

/**
 * @swagger
 * /apis/endGame:
 *      post:
 *          summary: Recebe o resultado de um jogo
 *          tags:
 *            - Game
 *          description: Recebe um json com o resultado de um jogo
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: string
 *                                  example: f2c98334-ea2d-4cf2-9862-51d34a8220d0
 *                              difficulty:
 *                                  type: string
 *                                  example: EASY
 *                              playerId1:
 *                                  type: string
 *                                  example: 1
 *                              playerId2:
 *                                  type: string
 *                                  example: null
 *                              status:
 *                                  type: integer
 *                                  example: 1
 *                              mode:
 *                                  type: integer
 *                                  example: 0
 *                              cookieCoins:
 *                                  type: integer
 *                                  example: 0
 *                              ingredients:
 *                                  type: array
 *                                  example: ["Agua","Beterraba","Boi","Cebola","Couve","Galinha","Hortelã","Ovo","Rabo","Sopa"]
 *                              time:
 *                                  type: integer
 *                                  example: 7
 *                              recipes:
 *                                  type: array
 *                                  example: [{"name":"Sopa de Rabo de Boi","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Boi","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]},{"name":"Sopa de Rabo de Galinha","difficulty":"EASY","payment":20,"ingredients":[{"name":"Agua","quantity":1},{"name":"Sopa","quantity":2},{"name":"Galinha","quantity":1},{"name":"Rabo","quantity":1}],"tasks":[{"name":"Lavar","time":"2"},{"name":"Cozer","time":"3"}]}]
 *                              result:
 *                                  type: integer
 *                                  example: 1
 *                              recipesDoneP1:
 *                                  type: string
 *                                  example: [{"recipe":"a","number":5},{"recipe":"b","number":10},{"recipe":"c","number":25},{"recipe":"d","number":15}]
 *                              recipesDoneP2:
 *                                  type: string
 *                                  example: [{"recipe":"a","number":25},{"recipe":"b","number":15},{"recipe":"g","number":10},{"recipe":"f","number":5}]
 *          responses:
 *              200:
 *                  description: OK
 */