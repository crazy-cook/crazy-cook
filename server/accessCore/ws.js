const ws = require('ws');
const playitApi = require('./api');
const { v4: uuidv4 } = require('uuid');
const openGame = require('../functions/openGame');
const payment = require('../functions/payment');
const {coreResult} = require("../functions/enums");
const {gameResult} = require("../functions/enums");
const {coreDifficulty} = require("../functions/enums");
const {gameDifficulty} = require("../functions/enums");
const {coreMode} = require("../functions/enums");
const {gameMode} = require("../functions/enums");

const gameId = process.env.PLAYITAPI_GAMEID;

/**
 * @module accessCore/ws
 */
/**
 * @type {Map<string, string>}
 * @memberOf module:accessCore/ws
 */
let requests = new Map();
let playitWs;
let connected = false;

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

/**
 * <hr><h4><b>connectWS</b>(connect)</h4>
 * @async
 * @param {boolean} connect
 * @return {Promise<WebSocket>}
 * @memberOf module:accessCore/ws
 */
async function connectWS (connect){
    if (connect){
        let token = '';

        let count = 0;
        while (true){
            if (count === 50) {
                throw 'Error getting token (ws)'
            }
            try {
                let tk = await playitApi.getToken();
                console.log(tk);
                token = tk;
                break;
            } catch (e) {
                count += 1;
                console.log(count)
                await delay(500)
            }
        }

        playitWs = new ws('ws://playitapi.website/');

        playitWs.on('open', ev => {
            let id = uuidv4();
            let json = {
                "event": "handshake",
                "request-id": id,
                "data": {
                    "token": token,
                    "game-id": gameId
                }
            }
            console.log(JSON.stringify(json));
            playitWs.send(JSON.stringify(json));
            requests.set(id, 'handshake');
        })

        playitWs.on('message', ev => {
            console.log(ev);
            let jsonEv = JSON.parse(ev);
            let msgEvent = jsonEv.event;
            if (msgEvent === 'start-match' || msgEvent === 'payment-status'){
                try {
                    switch (msgEvent) {
                        case 'start-match': {
                            openGame.openGame(jsonEv.data);
                            let json = {
                                "event": "success",
                                "request-id": jsonEv['request-id']
                            }
                            playitWs.send(JSON.stringify(json))
                            break
                        }
                        case 'payment-status': {
                            payment.receivePayment(jsonEv.data.payment)
                            let json = {
                                "event": "success",
                                "request-id": jsonEv['request-id']
                            }
                            playitWs.send(JSON.stringify(json))
                            break
                        }
                    }
                } catch (e) {
                    let json = {
                        "event": "error",
                        "request-id": jsonEv['request-id']
                    }
                    playitWs.send(JSON.stringify(json))
                }
            } else {
                let id = jsonEv['request-id'];
                let event = requests.get(id);
                requests.delete(id);
                if (event === 'handshake' && msgEvent === 'success') {
                    connected = true;
                    console.log('handshake ok');
                }
            }
        })

        playitWs.on('close', ev => async function () {
            connected = false;
            try {
                await connectWS(true);
            } catch (e) {
                throw 'Error on close WS'
            }
        })
    }
    if (!connect){
        if (connected){
            return playitWs;
        }else {
            if (!connected) {
                throw 'Error ws not connected'
            }
        }
    }
}

/**
 * <hr><h4><b>send</b>(jsonObj)</h4>
 * @async
 * @param {object} jsonObj
 * @return {Promise<void>}
 * @memberOf module:accessCore/ws
 */
async function send(jsonObj) {
    let count = 0;
    while (true){
        if (count === 50) {
            throw 'Error ws not connected'
        }
        try {
            let wsc = await connectWS(false);
            wsc.send(JSON.stringify(jsonObj));
            break;
        } catch (e) {
            count += 1;
            console.log(count);
            await delay(500)
        }
    }
}

/**
 * <hr><h4><b>convertDifficulty</b>(difficult)</h4>
 * @param {string} difficult
 * @return {string}
 * @memberOf module:accessCore/ws
 */
function convertDifficulty(difficult) {
    switch (difficult) {
        case gameDifficulty.EASY: return coreDifficulty.EASY;
        case gameDifficulty.MEDIUM: return coreDifficulty.MEDIUM;
        case gameDifficulty.HARD: return coreDifficulty.HARD;
    }
}

/**
 * <hr><h4><b>convertResult</b>(gameObj, isP1)</h4>
 * @param {object} gameObj
 * @param {boolean} isP1
 * @return {string}
 * @memberOf module:accessCore/ws
 */
function convertResult(gameObj, isP1) {
    if (gameObj.getMode() === gameMode.SINGLEPLAYER){
        switch (gameObj.getResult()) {
            case gameResult.DRAW: return coreResult.LOSE;
            case gameResult.P1WIN: return coreResult.WIN;
        }
    }
    if (gameObj.getMode() === gameMode.MULTIPLAYER && isP1){
        switch (gameObj.getResult()) {
            case gameResult.DRAW: return coreResult.DRAW;
            case gameResult.P1WIN: return coreResult.WIN;
            case gameResult.P2WIN: return coreResult.LOSE;
        }
    }
    if (gameObj.getMode() === gameMode.MULTIPLAYER && !isP1){
        switch (gameObj.getResult()) {
            case gameResult.DRAW: return coreResult.DRAW;
            case gameResult.P1WIN: return coreResult.LOSE;
            case gameResult.P2WIN: return coreResult.WIN;
        }
    }
}

/**
 * <hr><h4><b>sendEndAux</b>(jsonObj)</h4>
 * @param {object} jsonObj
 * @return {object}
 * @memberOf module:accessCore/ws
 */
function sendEndAux(jsonObj) {
    let id = uuidv4();
    requests.set(id, "match-end");
    return {
        "event": "match-end",
        "request-id": id,
        "data": {
            "game-id": gameId,
            "match-result": jsonObj
        }
    }
}

/**
 * <hr><h4><b>sendEnd</b>(gameObj)</h4>
 * @async
 * @param {object} gameObj
 * @param {boolean} isP1
 * @return {Promise<void>}
 * @memberOf module:accessCore/ws
 */
async function sendEnd(gameObj, isP1) {
    if (gameObj.getMode() === gameMode.SINGLEPLAYER){
        let json = {
            "user_id": gameObj.getPlayerId1(),
            "mode": coreMode.SINGLEPLAYER,
            "difficulty": convertDifficulty(gameObj.getDifficulty()),
            "result": convertResult(gameObj, true),
            "duration": gameObj.getTime()
        }
        console.log(sendEndAux(json))
        await send(sendEndAux(json));
    } else if (gameObj.getMode() === gameMode.MULTIPLAYER){
        if (isP1) {
            let json1 = {
                "user_id": gameObj.getPlayerId1(),
                "mode": coreMode.MULTIPLAYER,
                "difficulty": convertDifficulty(gameObj.getDifficulty()),
                "result": convertResult(gameObj, true),
                "duration": gameObj.getTime()
            }
            console.log(sendEndAux(json1))
            await send(sendEndAux(json1));
        } else {
            let json2 = {
                "user_id": gameObj.getPlayerId2(),
                "mode": coreMode.MULTIPLAYER,
                "difficulty": convertDifficulty(gameObj.getDifficulty()),
                "result": convertResult(gameObj, false),
                "duration": gameObj.getTime()
            }
            console.log(sendEndAux(json2))
            await send(sendEndAux(json2));
        }
    }
}

/**
 * <hr><h4><b>sendPublish</b>(playerId, msg)</h4>
 * @async
 * @param {string} playerId
 * @param {string} msg
 * @return {Promise<void>}
 * @memberOf module:accessCore/ws
 */
async function sendPublish(playerId, msg) {
    let id = uuidv4();
    let json = {
        "event": "publish",
        "request-id": id,
        "data": {
            "game-id": gameId,
            "publish": {
                "user_id": playerId,
                "text": msg
            }
        }
    }
    requests.set(id, "publish");
    await send(json);
}

async function teste() {
    let count = 0;
    while (true){
        if (count === 50) {
            throw 'Error ws not connected'
        }
        try {
            let wsc = await connectWS(false);
            console.log('readyState: ' + wsc.readyState.toString());
            break;
        } catch (e) {
            count += 1;
            console.log(count);
            await delay(500)
        }
    }
}

async function teste2() {
    await connectWS(true);
    await teste();
}

teste2()
//connectWS(true)

module.exports = {
    connectWS,
    sendEnd,
    sendPublish
}