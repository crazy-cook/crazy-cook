const axios = require('axios');

const playitApi = axios.create({
    baseURL: 'https://playitapi.website',
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 10000,
});

/**
 * @module accessCore/api
 */
/**
 * <hr><h4><b>getToken</b>()</h4>
 * @async
 * @return {string}
 * @memberOf module:accessCore/api
 */
async function getToken() {
    try {
        let resp = await playitApi.get('/session/get', {
            params: { 'game-id': process.env.PLAYITAPI_GAMEID },
            auth: {
                username: process.env.PLAYITAPI_USERNAME,
                password: process.env.PLAYITAPI_PASSWORD,
            }
        });
        if (resp.data.success) {
            return resp.data.token;
        } else {
            throw 'Error getting token (api)'
        }
    } catch (e) {
        throw 'Error getting token (api)'
    }
}

/**
 * <hr><h4><b>postPayment</b>(id, playerId, motive, value)</h4>
 * @async
 * @param {string} id
 * @param {string} playerId
 * @param {string} motive
 * @param {string} value
 * @return {Promise<boolean>}
 * @memberOf module:accessCore/api
 */
async function postPayment(id, playerId, motive, value) {
    try {
        let resp = await playitApi.post('/payment/request', {
            'game-id': process.env.PLAYITAPI_GAMEID,
            'user-id': playerId,
            'transaction-id': id,
            motive: motive,
            value: value
        }, {
            auth: {
                username: process.env.PLAYITAPI_USERNAME,
                password: process.env.PLAYITAPI_PASSWORD,
            }
        });
        return resp.data.success;
    } catch (e) {
        throw 'Error posting payment'
    }
}

module.exports = {
    getToken,
    postPayment
}