/**
 * @class task
 * @memberOf models
 * @description task<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(task)<br><br>
 */
let task = {
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa a task, a partir de um JSON
     * @memberOf models.task
     * @param {object} json - JSON Object
     */
    fromJson: function(json) {
        this.name = json.name;
        this.time = json.time;
    },
    getName: function () {
        return this.name;
    },
    getTime: function () {
        return this.time;
    }
};

module.exports = task;