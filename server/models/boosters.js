const { getOne, putOne } = require("../accessDB/boostersDB");
/**
 * @class boosters
 * @memberOf models
 * @description Boosters<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(boosters)<br><br>
 */
let boosters = {
    /**
     * <hr><h4><b>init</b>(playerId, nBoosters)</h4><br>Inicializa o boosters
     * @param {string} playerId
     * @param {number} nBoosters
     * @memberOf models.boosters
     */
    init: function (playerId, nBoosters) {
        this.playerId = playerId;
        this.nBoosters = nBoosters
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa o boosters, a partir de um JSON
     * @memberOf models.boosters
     * @param {object} json
     */
    fromJson: function(json) {
        this.playerId = json.playerId;
        this.nBoosters = json.nBoosters;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON do boosters
     * @memberOf models.boosters
     * @return {object} json
     */
    toJson: function() {
        return {
            playerId: this.playerId,
            nBoosters: this.nBoosters
        };
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva o boosters na db
     * @memberOf models.boosters
     */
    save: function() {
        return putOne(this)
    },
    /**
     * <hr><h4><b>getFromDB</b>(playerId)</h4><br>Inicializa o boosters, a partir da db
     * @async
     * @memberOf models.boosters
     * @param {string} playerId
     * @return {object} boosters
     */
    getFromDB: async function(playerId) {
        await getOne(playerId).then(json => {
            this.fromJson(json);
        }).catch(err => {
            this.init(playerId, 0);
            this.save()
        })
        return this;
    }
}