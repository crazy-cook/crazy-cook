const {getOne} = require("../accessDB/difficultyDB");
/**
 * @class difficulty
 * @memberOf models
 * @description Game difficulty<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(difficulty)<br><br>
 */
let difficulty = {
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa a difficulty, a partir de um JSON
     * @memberOf models.difficulty
     * @param {object} json - JSON Object
     */
    fromJson: function(json) {
        this.name = json.name;
        this.time = json.time;
        this.numberOfRecipes = json.numberOfRecipes;
        this.minIngredientsNumber = json.minIngredientsNumber;
        this.minRandomsNumber = json.minRandomsNumber;
    },
    /**
     * <hr><h4><b>getFromDB</b>(playerId)</h4><br>Inicializa a difficulty, a partir da db
     * @async
     * @memberOf models.difficulty
     * @param {string} gameDifficulty - Enum gameDifficulty
     * @return {object} difficulty - Objeto da Class Difficulty
     */
    getFromDB: async function(gameDifficulty) {
        await getOne(gameDifficulty).then(json => {
            this.fromJson(json);
        })
        return this;
    },
    getName: function() {
        return this.name;
    },
    getTime: function() {
        return this.time;
    },
    getNumberOfRecipes: function() {
        return this.numberOfRecipes;
    },
    getMinIngredientsNumber: function() {
        return this.minIngredientsNumber;
    },
    getMinRandomsNumber: function() {
        return this.minRandomsNumber;
    }
};

module.exports = difficulty;