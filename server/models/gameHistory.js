const { putOne } = require("../accessDB/gameHistoryDB");
const { gameResult } = require("../functions/enums")
/**
 * @class gameHistory
 * @memberOf models
 * @description gameHistory<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(gameHistory)<br><br>
 */
let gameHistory = {
    /**
     * <hr><h4><b>init</b>(gameJson, nRecipesDone, bestRecipe, player2Name)</h4><br>Inicializa o game history
     * @memberOf models.gameHistory
     * @param {object} gameJson
     * @param {number} nRecipesDone
     * @param {string} bestRecipe
     * @param {(string|null)} player2Name
     */
    init: function(gameJson, nRecipesDone, bestRecipe, player2Name, isP1) {
        this.id = gameJson.id; //id = gameID

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        this.date = dd + '/' + mm + '/' + yyyy;
        this.gameMode = gameJson.mode;
        this.gameDifficulty = gameJson.difficulty;
        this.time = gameJson.time;
        if (gameJson.isP1)
            this.cookieCoins = gameJson.cookieCoinsP1;
        else
            this.cookieCoins = gameJson.cookieCoinsP2;
        this.nRecipesDone = nRecipesDone;
        this.bestRecipe = bestRecipe;
        if (isP1){
            switch (gameJson.result) {
                case gameResult.DRAW:
                    this.gameResult = "Empate";
                    break
                case gameResult.P1WIN:
                    this.gameResult = "Vitória";
                    break
                case gameResult.P2WIN:
                    this.gameResult = "Derrota";
                    break
            }
        } else {
            switch (gameJson.result) {
                case gameResult.DRAW:
                    this.gameResult = "Empate";
                    break
                case gameResult.P1WIN:
                    this.gameResult = "Derrota";
                    break
                case gameResult.P2WIN:
                    this.gameResult = "Vitória";
                    break
            }
        }
        this.playerId1 = gameJson.playerId1;
        this.playerName2 = player2Name;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON do game history
     * @memberOf models.gameHistory
     * @return {object} json
     */
    toJson: function () {
        return {
            id: this.id,
            date: this.date,
            gameMode: this.gameMode,
            gameDifficulty: this.gameDifficulty,
            time: this.time,
            cookieCoins: this.cookieCoins,
            nRecipesDone: this.nRecipesDone,
            bestRecipe: this.bestRecipe,
            gameResult: this.gameResult,
            playerId1: this.playerId1,
            playerName2: this.playerName2
        }
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva o game history na db
     * @memberOf models.gameHistory
     */
    save: function() {
        return putOne(this);
    }
};

module.exports = gameHistory;