const {getOne, putOne} = require("../accessDB/recipeDB");
const { v4: uuidv4 } = require('uuid');
/**
 * @class recipe
 * @memberOf models
 * @description recipe<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(recipe)<br><br>
 */
let recipe = {
    /**
     * <hr><h4><b>init</b>(name, ingredients, difficulty, tasks, payment)</h4><br>Inicializa a recipe
     * @memberOf models.recipe
     * @param {string} name
     * @param {object[]} ingredients
     * @param {string} difficulty
     * @param {object[]} tasks
     * @param {number} payment
     */
    init: function(name, ingredients, difficulty, tasks, payment) {
        this.id = uuidv4();
        this.name = name;
        this.ingredients = ingredients;
        this.difficulty = difficulty;
        this.tasks = tasks;
        this.payment = payment;
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa a recipe, a partir de um JSON
     * @memberOf models.recipe
     * @param {object} json - JSON Object
     */
    fromJson: function(json) {
        const ingredientClass = require('./ingredient.js')
        const taskClass = require('./task.js')

        this.name = json.name;
        this.difficulty = json.difficulty;
        this.payment = json.payment;

        this.ingredients = [];
        json.ingredients.forEach(element => {
            let ingredient = Object.create(ingredientClass);
            ingredient.fromJson(element);
            this.ingredients.push(ingredient);
        });

        this.tasks = [];
        json.tasks.forEach(element => {
            let task = Object.create(taskClass);
            task.fromJson(element);
            this.tasks.push(task);
        });
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva a recipe na db
     * @memberOf models.recipe
     */
    save: function() {
        return putOne(this)
    },
    /**
     * <hr><h4><b>getFromDB</b>(id)</h4><br>Inicializa a recipe, a partir da db
     * @async
     * @memberOf models.recipe
     * @param {string} id
     * @return {object} recipe
     */
    getFromDB: async function(id) {
        await getOne(id).then(json => {
            this.fromJson(json);
        })
        return this;
    },
    getId: function() {
        return this.id;
    },
    getName: function() {
        return this.name;
    },
    getIngredients: function() {
        return this.ingredients;
    },
    getDifficult: function() {
        return this.difficulty;
    },
    getTasks: function() {
        return this.tasks;
    },
    getPayment: function() {
        return this.payment;
    }
};

module.exports = recipe;