const { getOne, putOne } = require("../accessDB/playerDB");
/**
 * @class player
 * @memberOf models
 * @description Player<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(player)<br><br>
 */
let player = {
    /**
     * <hr><h4><b>init</b>(id, name, avatar, email, birthdate)</h4><br>Inicializa o player
     * @memberOf models.player
     * @param {string} id
     * @param {string} name
     * @param {string} avatar
     * @param {string} email
     * @param {string} birthdate
     */
    init: function(id, name, avatar, email, birthdate) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.email = email;
        this.birthdate = birthdate;
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa o player, a partir de um JSON
     * @memberOf models.player
     * @param {object} json
     */
    fromJson: function(json) {
        this.id = json.id;
        this.name = json.name;
        this.avatar = json.avatar;
        this.email = json.email;
        this.birthdate = json.birthdate;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON do player
     * @memberOf models.player
     * @return {object} json
     */
    toJson: function() {
        return {
            id: this.id,
            name: this.name,
            avatar: this.avatar,
            email: this.email,
            birthdate: this.birthdate
        };
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva o player na db
     * @memberOf models.player
     */
    save: function() {
        return putOne(this)
    },
    /**
     * <hr><h4><b>getFromDB</b>(id)</h4><br>Inicializa o player, a partir da db
     * @async
     * @memberOf models.player
     * @param {string} id
     * @return {object} player
     */
    getFromDB: async function(id) {
        await getOne(id).then(json => {
            this.fromJson(json);
        })
        return this;
    },
    getId: function() {
        return this.id;
    },
    getName: function() {
        return this.name;
    },
    getAvatar: function() {
        return this.avatar;
    },
    getBirthdate: function() {
        return this.birthdate;
    }
};

module.exports = player;