const { getOne, putOne } = require("../accessDB/configDB");

/**
 * @class config
 * @memberOf models
 * @description User configuration<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(config)<br><br>
 */
let config = {
    /**
     * <hr><h4><b>init</b>(playerId, theme)</h4><br>Inicializa a config
     * @memberOf models.config
     * @param {string} playerId - Identificação única do player
     * @param {number} theme
     */
    init: function(playerId, theme) {
        this.playerid = playerId;
        this.theme = theme;
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa a config, a partir de um JSON
     * @memberOf models.config
     * @param {object} json - JSON Object
     */
    fromJson: function(json) {
        this.playerid = json.playerid;
        this.theme = json.theme;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON da config
     * @memberOf models.config
     * @return {object} json - JSON Object
     */
    toJson: function() {
        return {
            playerid: this.playerid,
            theme: this.theme
        }
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva a config na db
     * @memberOf models.config
     */
    save: function() {
        return putOne(this)
    },
    /**
     * <hr><h4><b>getFromDB</b>(playerId)</h4><br>Inicializa a config, a partir da db
     * @async
     * @memberOf models.config
     * @param {string} playerId - Identificação única do player
     * @return {object} config - Objeto da Class Config
     */
    getFromDB: async function(playerId) {
        await getOne(playerId).then(json => {
            if (!json) {
                this.init(playerId, 0)
            } else {
                this.fromJson(json);
            }
        })
        return this;
    },
    getPlayerId: function() {
        return this.playerid;
    },
    getTheme: function() {
        return this.theme;
    }
};

module.exports = config;