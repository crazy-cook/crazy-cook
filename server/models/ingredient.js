/**
 * @class ingredient
 * @memberOf models
 * @description Ingredient<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(ingredient)<br><br>
 */
let ingredient = {
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa o ingredient, a partir de um JSON
     * @memberOf models.ingredient
     * @param {object} json
     */
    fromJson: function(json) {
        this.name = json.name;
        this.quantity = json.quantity;
    },
    getName: function () {
        return this.name;
    },
    getQuantity: function () {
        return this.quantity;
    }
};

module.exports = ingredient;