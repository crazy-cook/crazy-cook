const { putOne } = require("../accessDB/rankDB");
/**
 * @class rank
 * @memberOf models
 * @description rank<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(rank)<br><br>
 */
let rank = {
    /**
     * <hr><h4><b>init</b>(playerId, playerName, coins, nGamesSingle, nGamesMulti, nMultiWin, nMultiLoss, totalRecipes)</h4><br>Inicializa o rank
     * @memberOf models.rank
     * @param {string} playerId - Identificação única do player
     * @param {string} playerName
     * @param {number} coins
     * @param {number} nGamesSingle
     * @param {number} nGamesMulti
     * @param {number} nMultiWin
     * @param {number} nMultiLoss
     * @param {number} totalRecipes
     */
    init: function(playerId, playerName, coins, nGamesSingle, nGamesMulti, nMultiWin, nMultiLoss, totalRecipes) {
        this.id = id; //id = playerid
        this.playerName = playerName;
        this.totalCoins = coins;
        this.nGamesSingle = nGamesSingle;
        this.nGamesMulti = nGamesMulti;
        this.nGames = nGamesSingle + nGamesMulti;
        this.nMultiWin = nMultiWin;
        this.nMultiLoss = nMultiLoss;
        this.nMultiDraw = this.nGames - (nMultiLoss + nMultiWin);
        this.totalRecipes = totalRecipes;
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa o rank, a partir de um JSON
     * @memberOf models.rank
     * @param {object} json
     */
    fromJson: function(json) {
        this.id = json.id; //id = playerid
        this.playerName = json.playerName;
        this.totalCoins = json.totalCoins;
        this.nGamesSingle = json.nGamesSingle;
        this.nGamesMulti = json.nGamesMulti;
        this.nGames = json.nGamesSingle + json.nGamesMulti;
        this.nMultiWin = json.nMultiWin;
        this.nMultiLoss = json.nMultiLoss;
        this.nMultiDraw = json.nGamesMulti - json.nMultiWin - json.nMultiLoss;
        this.totalRecipes = json.totalRecipes;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON do rank
     * @memberOf models.rank
     * @return {object} json
     */
    toJson: function () {
        return {
            id: this.id,
            playerName: this.playerName,
            totalCoins: this.totalCoins,
            nGamesSingle: this.nGamesSingle,
            nGamesMulti: this.nGamesMulti,
            nMultiWin: this.nMultiWin,
            nMultiLoss: this.nMultiLoss,
            totalRecipes: this.totalRecipes,
        }
    },
    /**
     * <hr><h4><b>toJsonFront</b>()</h4><br>Retorna um JSON do rank para enviar na Api
     * @memberOf models.rank
     * @return {object} json
     */
    toJsonFront: function () {
        return {
            id: this.id,
            playerName: this.playerName,
            totalCoins: this.totalCoins,
            nGamesSingle: this.nGamesSingle,
            nGamesMulti: this.nGamesMulti,
            nGames: this.nGames,
            nMultiWin: this.nMultiWin,
            nMultiLoss: this.nMultiLoss,
            nMultiDraw: this.nMultiDraw,
            totalRecipes: this.totalRecipes,
            position: this.position
        }
    },
    /**
     * <hr><h4><b>toJsonSimple</b>()</h4><br>Retorna um JSON (simplificado) do rank
     * @memberOf models.rank
     * @return {object} json
     */
    toJsonSimple: function () {
        return {
            playerName: this.playerName,
            totalCoins: this.totalCoins,
            position: this.position
        }
    },
    setPosition: function (position) {
        this.position = position;
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva o rank na db
     * @memberOf models.rank
     */
    save: function() {
        return putOne(this);
    }
};

module.exports = rank;