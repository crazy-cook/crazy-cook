const { putOne, getOne, getOneRandom } = require("../accessDB/gameDB");
const { gameStatus } = require("../functions/enums");
const { v4: uuidv4 } = require('uuid');
/**
 * @class game
 * @memberOf models
 * @description Game<br><br><h5>Pattern:</h5>OLOO (Objects Linked to Other Objects)<br><br><h5>New Instance:</h5>Object.create(game)<br><br>
 */
let game = {
    /**
     * <hr><h4><b>init</b>(playerId1, playerId2, status, mode, difficulty)</h4><br>Inicializa o game
     * @memberOf models.game
     * @param {string} playerId1 - Identificação única do player 1
     * @param {(string|null)} playerId2 - Identificação única do player 2
     * @param {number} status
     * @param {number} mode
     * @param {string} difficulty
     */
    init: function(playerId1, playerId2, status, mode, difficulty) {
        this.id = uuidv4();
        this.difficulty = difficulty;
        this.playerId1 = playerId1;
        this.playerId2 = playerId2;
        this.status = status;
        this.mode = mode;
        this.cookieCoinsP1 = 0;
        this.ingredients = [];
    },
    /**
     * <hr><h4><b>fromJson</b>(json)</h4><br>Inicializa o game, a partir de um JSON
     * @memberOf models.game
     * @param {object} json - JSON Object
     */
    fromJson: function(json) {
        this.id = json.id;
        this.playerId1 = json.playerId1;
        this.playerId2 = json.playerId2;
        this.status = json.status;
        this.mode = json.mode;
        this.difficulty = json.difficulty;
        this.cookieCoinsP1 = json.cookieCoinsP1;
        this.cookieCoinsP2 = json.cookieCoinsP2;
        this.ingredients = json.ingredients;
        this.recipes = json.recipes;
        this.time = json.time;
    },
    /**
     * <hr><h4><b>fromEndJson</b>(json)</h4><br>Inicializa o game, a partir de um JSON de fim de jogo
     * @memberOf models.game
     * @param {object} json - JSON Object
     */
    fromEndJson: function(json) {
        this.fromJson(json);
        this.result = json.result;
        this.recipesDoneP1 = json.recipesDoneP1;
        this.recipesDoneP2 = json.recipesDoneP2;
        this.isP1 = json.isP1;
    },
    /**
     * <hr><h4><b>toJson</b>()</h4><br>Retorna um JSON do game
     * @memberOf models.game
     * @return {object} json - JSON Object
     */
    toJson: function() {
        return {
            id: this.id,
            playerId1: this.playerId1,
            playerId2: this.playerId2,
            status: this.status,
            mode: this.mode,
            difficulty: this.difficulty,
            cookieCoinsP1: this.cookieCoinsP1,
            cookieCoinsP2: this.cookieCoinsP2,
            recipes: this.recipes,
            ingredients: this.ingredients,
            time: this.time
        };
    },
    setPlayerId2: function(playerId2) {
        this.playerId2 = playerId2;
        this.cookieCoinsP2 = 0;
        this.status = gameStatus.START;
    },
    setRecipes: function(recipes) {
        this.recipes = recipes;
    },
    setIngredients: function(ingredients) {
        this.ingredients = ingredients;
    },
    setCookieCoinsP1: function(cookieCoinsP1) {
        this.cookieCoinsP1 = cookieCoinsP1;
    },
    setCookieCoinsP2: function(cookieCoinsP2) {
        this.cookieCoinsP2 = cookieCoinsP2;
    },
    setTime: function(time) {
        this.time = time;
    },
    setResult: function(result) {
        this.result = result;
    },
    setRecipesDoneP1: function (jsonArray) {
        this.recipesDoneP1 = jsonArray;
    },
    setRecipesDoneP2: function(jsonArray) {
        this.recipesDoneP2 = jsonArray;
    },
    setStatus: function (status) {
        this.status = status
    },
    /**
     * <hr><h4><b>save</b>()</h4><br>Salva o game na db
     * @memberOf models.game
     */
    save: async function() {
        return await putOne(this);
    },
    /**
     * <hr><h4><b>getFromDB</b>(playerId)</h4><br>Inicializa o game, a partir da db
     * @async
     * @memberOf models.game
     * @param {string} id
     * @return {object} game
     */
    getFromDB: async function(id) {
        await getOne(id).then(json => {
            this.fromJson(json);
        })
        return this;
    },
    /**
     * <hr><h4><b>getRandomFromDB</b>(playerId2, status, mode)</h4>
     * @async
     * @param {(string|null)} playerId2
     * @param {number} status
     * @param {number} mode
     * @return {Promise<models.game>}
     * @memberOf models.game
     */
    getRandomFromDB: async function(playerId2, status, mode) {
        await getOneRandom(playerId2, status, mode).then(json => {

            if (json)
                this.fromJson(json);
        })
        return this;
    },

    //gets
    getId: function() {
        return this.id;
    },
    getPlayerId1: function() {
        return this.playerId1;
    },
    getPlayerId2: function() {
        return this.playerid2;
    },
    getRecipes: function() {
        return this.recipes;
    },
    getIngredients: function() {
        return this.ingredients;
    },
    getStatus: function() {
        return this.status;
    },
    getMode: function() {
        return this.mode;
    },
    getDifficulty: function() {
        return this.difficulty;
    },
    getCookieCoinsP1: function() {
        return this.cookieCoinsP1;
    },
    getCookieCoinsP2: function() {
        return this.cookieCoinsP2;
    },
    getTime: function() {
        return this.time;
    },
    getResult: function() {
        return this.result;
    }
};

module.exports = game;