const WebSocket = require('ws');

function noop() {}

function heartbeat() {
    this.isAlive = true;
}

function getId(gameId, isP1) {
    if (isP1) {
        return gameId + '_1'
    } else {
        return gameId + '_2'
    }
}

let interval;

const wss = new WebSocket.Server({ port: 8088 });
let clients = new Map();

wss.on('connection', function connection(ws) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);

    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
        let msg = JSON.parse(message);

        if (!msg.isMove && !msg.isCoins){
            let id = getId(msg.gameId, msg.isP1);
            ws.id = id;
            clients.set(id, ws);
        } else {
            let json;
            if (!msg.isCoins) {
                json = {
                    isMove: true,
                    move: msg.move
                }
            } else {
                json = {
                    isMove: false,
                    move: msg.move
                }
            }
            let id = getId(msg.gameId, !msg.isP1);
            clients.get(id).send(JSON.stringify(json))
        }
    });

    interval = setInterval(function ping() {
        wss.clients.forEach(function each(wsc) {
            if (wsc.isAlive === false) {
                clients.delete(wsc.id);
                return wsc.terminate();
            }

            wsc.isAlive = false;
            wsc.ping(noop);
        });
    }, 30000);
});

wss.on('close', function close() {
    clearInterval(interval);
});