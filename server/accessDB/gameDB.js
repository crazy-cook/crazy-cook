const connection = require("../connection");

/**
 * @module accessDB/gameDB
 */
/**
 * <hr><h4><b>getOne</b>(id)</h4>
 * @async
 * @param {string} id
 * @return {Promise<object>}
 * @memberOf module:accessDB/gameDB
 */
async function getOne(id) {
    let one;
    await connection.search({id: id}, 'game').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>getAll</b>()</h4>
 * @async
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/gameDB
 */
async function getAll() {
    let all;
    await connection.search({}, 'game').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>getOneRandom</b>(playerId2, status, mode)</h4>
 * @async
 * @param {string} playerId2
 * @param {number} status
 * @param {number} mode
 * @return {Promise<object>}
 * @memberOf module:accessDB/gameDB
 */
async function getOneRandom(playerId2, status, mode) {
    let one;
    await connection.search({ playerId2: playerId2, status: status, mode: mode }, 'game').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>putOne</b>(game)</h4>
 * @async
 * @param {object} game
 * @return {Promise<void>}
 * @memberOf module:accessDB/gameDB
 */
async function putOne(game) {
    return connection.save('game', game.id, game);
}

module.exports = {
    getOne,
    getAll,
    getOneRandom,
    putOne
};