const connection = require("../connection");

/**
 * @module accessDB/boostersDB
 */
/**
 * <hr><h4><b>getOne</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object>}
 * @memberOf module:accessDB/boostersDB
 */
async function getOne(playerId) {
    let one;
    await connection.search({ playerId: playerId }, 'booster').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>putOne</b>(boosters)</h4>
 * @async
 * @param {object} boosters
 * @return {Promise<void>}
 * @memberOf module:accessDB/boostersDB
 */
async function putOne(boosters) {
    return connection.save('booster', boosters.playerId, boosters);
}

module.exports = {
    getOne,
    putOne
};