const connection = require("../connection");

/**
 * @module accessDB/rankDB
 */
/**
 * <hr><h4><b>getOne</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object>}
 * @memberOf module:accessDB/rankDB
 */
async function getOne(playerId) {
    let one;
    await connection.search({ id: playerId }, 'rank').then(s => {
        one = s[0];
    }).catch(err => {
        one = []
    });
    return one;
}

/**
 * <hr><h4><b>getAll</b>()</h4>
 * @async
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/rankDB
 */
async function getAll() {
    let all;
    await connection.search({}, 'rank').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>putOne</b>(rank)</h4>
 * @async
 * @param {object} rank
 * @return {Promise<void>}
 * @memberOf module:accessDB/rankDB
 */
async function putOne(rank) {
    return connection.save('rank', rank.id, rank);
}

module.exports = {
    getOne,
    getAll,
    putOne
};