const connection = require("../connection");

/**
 * @module accessDB/sessionDB
 */
/**
 * <hr><h4><b>getOne</b>(id)</h4>
 * @async
 * @param {string} id
 * @return {Promise<object>}
 * @memberOf module:accessDB/sessionDB
 */
async function getOne(id) {
    let one;
    await connection.search({id: id}, 'session').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>putOne</b>(session)</h4>
 * @async
 * @param {object} session
 * @return {Promise<void>}
 * @memberOf module:accessDB/sessionDB
 */
async function putOne(session) {
    return connection.save('session', session.id, session);
}

module.exports = {
    getOne,
    putOne
};