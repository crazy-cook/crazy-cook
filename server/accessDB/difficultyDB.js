const connection = require("../connection");

/**
 * @module accessDB/difficultyDB
 */
/**
 * <hr><h4><b>getOne</b>(gameDifficulty)</h4>
 * @async
 * @param {string} gameDifficulty
 * @return {Promise<object>}
 * @memberOf module:accessDB/difficultyDB
 */
async function getOne(gameDifficulty) {
    let one;
    await connection.search({ name: gameDifficulty }, 'difficulty').then(s => {
        one = s[0];
    });
    return one;
}

module.exports = {
    getOne
};