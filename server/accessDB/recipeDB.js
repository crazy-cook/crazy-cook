const connection = require("../connection");

/**
 * @module accessDB/recipeDB
 */
/**
 * <hr><h4><b>getOne</b>(id)</h4>
 * @async
 * @param {string} id
 * @return {Promise<object>}
 * @memberOf module:accessDB/recipeDB
 */
async function getOne(id) {
    let one;
    await connection.search({ id: id }, 'recipe').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>getByDifficulty</b>(difficulty)</h4>
 * @async
 * @param {string} difficulty
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/recipeDB
 */
async function getByDifficulty(difficulty) {
    let dif;
    await connection.search({ difficulty: difficulty }, 'recipe').then(s => {
        dif = s;
    });
    return dif;
}

/**
 * <hr><h4><b>getAll</b>()</h4>
 * @async
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/recipeDB
 */
async function getAll() {
    let all;
    await connection.search({}, 'recipe').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>putOne</b>(recipe)</h4>
 * @async
 * @param {object} recipe
 * @return {Promise<void>}
 * @memberOf module:accessDB/recipeDB
 */
async function putOne(recipe) {
    return connection.save('recipe', recipe.id, recipe);
}

module.exports = {
    getOne,
    getByDifficulty,
    getAll,
    putOne
};