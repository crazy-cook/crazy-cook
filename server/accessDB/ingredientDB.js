const connection = require("../connection");

/**
 * @module accessDB/ingredientDB
 */
/**
 * <hr><h4><b>getAll</b>()</h4>
 * @async
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/ingredientDB
 */
async function getAll() {
    let all;
    await connection.search({}, 'ingredient').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>putOne</b>(ingredient)</h4>
 * @async
 * @param {object} ingredient
 * @return {Promise<void>}
 * @memberOf module:accessDB/ingredientDB
 */
async function putOne(ingredient) {
    return connection.save('ingredient', null, ingredient);
}

module.exports = {
    getAll,
    putOne
};