const connection = require("../connection");

/**
 * @module accessDB/configDB
 */
/**
 * <hr><h4><b>getOne</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object>}
 * @memberOf module:accessDB/configDB
 */
async function getOne(playerId) {
    let one;
    await connection.search({ playerid: playerId }, 'config').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>putOne</b>(config)</h4>
 * @async
 * @param {object} config
 * @return {Promise<void>}
 * @memberOf module:accessDB/configDB
 */
async function putOne(config) {
    return connection.save('config', config.playerid, config);
}

module.exports = {
    getOne,
    putOne
};