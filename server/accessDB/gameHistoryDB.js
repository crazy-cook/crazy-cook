const connection = require("../connection");

/**
 * @module accessDB/gameHistoryDB
 */
/**
 * <hr><h4><b>getAll</b>()</h4>
 * @async
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/gameHistoryDB
 */
async function getAll() {
    let all;
    await connection.search({}, 'gameHistory').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>getAllByPlayerId</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object[]>}
 * @memberOf module:accessDB/gameHistoryDB
 */
async function getAllByPlayerId(playerId) {
    let all;
    await connection.search({ playerId1: playerId }, 'gameHistory').then(s => {
        all = s;
    });
    return all;
}

/**
 * <hr><h4><b>putOne</b>(gameHistory)</h4>
 * @async
 * @param {object} gameHistory
 * @return {Promise<void>}
 * @memberOf module:accessDB/gameHistoryDB
 */
async function putOne(gameHistory) {
    return connection.save('gameHistory', gameHistory.id, gameHistory);
}

module.exports = {
    getAll,
    getAllByPlayerId,
    putOne
}