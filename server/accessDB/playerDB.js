const connection = require("../connection");

/**
 * @module accessDB/playerDB
 */
/**
 * <hr><h4><b>getOne</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object>}
 * @memberOf module:accessDB/playerDB
 */
async function getOne(playerId) {
    let one;
    await connection.search({ id: playerId }, 'player').then(s => {
        one = s[0];
    });
    return one;
}

/**
 * <hr><h4><b>putOne</b>(playerObj)</h4>
 * @async
 * @param {object} playerObj
 * @return {Promise<void>}
 * @memberOf module:accessDB/playerDB
 */
async function putOne(playerObj) {
    return connection.save('player', playerObj.getId(), playerObj);
}

module.exports = {
    getOne,
    putOne
};