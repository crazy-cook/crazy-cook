const game = require("../models/game.js");
const playAux = require("./playAux.js");
const rank = require('./rank');
const gameHistory = require('./gameHistory');
const difficulty = require('../models/difficulty.js');
const { gameStatus, gameMode, gameDifficulty } = require("./enums.js");
const ws = require('../accessCore/ws');

/**
 * @module functions/play
 */
/**
 * <hr><h4><b>createGame</b>(playerId, mode, difficultyMode)</h4><br>Função inicializar o jogo
 * @async
 * @param {string} playerId
 * @param {number} mode
 * @param {string} difficultyMode
 * @return {Promise<{object}>}
 * @memberOf module:functions/play
 */
async function createGame(playerId, mode, difficultyMode) {

    let game1 = Object.create(game);

    if (mode === gameMode.MULTIPLAYER) {
        await game1.getRandomFromDB(null, gameStatus.WAIT, gameMode.MULTIPLAYER);
    }

    if (game1.id) {
        game1.setPlayerId2(playerId);

        await game1.save();

        return game1;
    } else {

        let difficultyGame = Object.create(difficulty);
        let status;

        if (mode === gameMode.SINGLEPLAYER) {

            status = gameStatus.START;

        } else {
            status = gameStatus.WAIT;
            difficultyMode = gameDifficulty.MEDIUM;
        }

        await game1.init(playerId, null, status, mode, difficultyMode);

        await difficultyGame.getFromDB(difficultyMode);

        game1.setTime(difficultyGame.time);

        try {
            let arrayRecipes = await playAux.getRecipes(game1.difficulty, difficultyGame.numberOfRecipes);
            game1.setRecipes(arrayRecipes);

            let ingredients = await playAux.getIngredients(arrayRecipes, difficultyGame.minIngredientsNumber, difficultyGame.minRandomsNumber);

            game1.setIngredients(ingredients)

            await game1.save();

            return game1;
        } catch (e) {
            console.log(e);
        }
    }
}

/**
 * <hr><h4><b>cancelGame</b>(gameJson)</h4>
 * @async
 * @param {object} gameJson
 * @return {Promise<void>}
 * @memberOf module:functions/play
 */
async function endGame(gameJson) {
    let gameObj = Object.create(game);
    gameObj.fromEndJson(gameJson);
    if (gameObj.mode === gameMode.SINGLEPLAYER) {
        await ws.sendEnd(gameObj, true);
        await rank.setRank(gameObj, true);
        await gameHistory.setGameHistory(gameObj, true);
    } else if (gameObj.mode === gameMode.MULTIPLAYER) {
        if (gameObj.isP1) {
            await ws.sendEnd(gameObj, true);
            await rank.setRank(gameObj, true);
            await gameHistory.setGameHistory(gameObj, true);
        } else {
            await ws.sendEnd(gameObj, false);
            await rank.setRank(gameObj, false);
            await gameHistory.setGameHistory(gameObj, false);
        }
    }
}

module.exports = {
    createGame,
    endGame
};