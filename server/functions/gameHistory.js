const gameHistoryClass = require('../models/gameHistory');
const player = require('../models/player');
const { gameMode } = require("./enums");

/**
 * @module functions/gameHistory
 */
/**
 * <hr><h4><b>compareRecipes</b>(a, b)</h4>
 * @param {object} a
 * @param {object} b
 * @return {number}
 * @memberOf module:functions/gameHistory
 */
function compareRecipes(a, b) {
    if (a.number < b.number) return -1;
    if (a.number > b.number) return 1;
    return 0;
}

/**
 * <hr><h4><b>setGameHistory</b>(gameObj, isP1)</h4>
 * @async
 * @param {object} gameObj
 * @param {boolean} isP1
 * @return {Promise<void>}
 * @memberOf module:functions/gameHistory
 */
async function setGameHistory(gameObj, isP1) {
    let bestRecipe, nRecipesDone
    if (isP1) {
        if (gameObj.recipesDoneP1.length !== 0) {
            gameObj.recipesDoneP1.sort(compareRecipes);
            gameObj.recipesDoneP1.reverse();
            console.log(gameObj.recipesDoneP1[0].recipe);

            bestRecipe = gameObj.recipesDoneP1[0].name;
        } else {
            bestRecipe = null
        }

        nRecipesDone = gameObj.recipesDoneP1.length;
    } else {
        if (gameObj.recipesDoneP2.length !== 0) {
            gameObj.recipesDoneP2.sort(compareRecipes);
            gameObj.recipesDoneP2.reverse();

            bestRecipe = gameObj.recipesDoneP2[0].name;
        } else {
            bestRecipe = null
        }

        nRecipesDone = gameObj.recipesDoneP2.length;
    }
    let player2 = Object.create(player);
    let player2Name;

    if (gameObj.mode === gameMode.MULTIPLAYER && isP1) {
        player2Name = player2.getFromDB(gameObj.playerId2);
    }
    if (gameObj.mode === gameMode.MULTIPLAYER && !isP1) {
        player2Name = player2.getFromDB(gameObj.playerId1);
    }

    let gameHistory = Object.create(gameHistoryClass);

    gameHistory.init(gameObj, nRecipesDone, bestRecipe, player2Name, isP1);

    await gameHistory.save();

}

module.exports = {
    setGameHistory
};