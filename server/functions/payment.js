const playitApi = require('../accessCore/api');
const { v4: uuidv4 } = require('uuid');

let paymentsMap = new Map();

/**
 * @module functions/payment
 */
/**
 * <hr><h4><b>requestPayment</b>(playerId, motive, value, res)</h4>
 * @async
 * @param {string} playerId
 * @param {string} motive
 * @param {string} value
 * @param {object} res
 * @return {Promise<void>}
 * @memberOf module:functions/payment
 */
async function requestPayment(playerId, motive, value, res) {
    let id = uuidv4();
    await playitApi.postPayment(id, playerId, motive, value).then(result => {
        if (result) {
            paymentsMap.set(id, res)
        } else {
            throw "Error payment Core Api"
        }
    })
}

/**
 * <hr><h4><b>receivePayment</b>(json)</h4>
 * @async
 * @param {object} json
 * @return {Promise<void>}
 * @memberOf module:functions/payment
 */
async function receivePayment(json) {
    let res = paymentsMap.get(json.id);
    if (json.status) {
        res.sendStatus(200);
    } else {
        res.sendStatus(400);
    }
}

module.exports = {
    requestPayment,
    receivePayment
}