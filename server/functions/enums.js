//Enum
/**
 * @module enums
 */
/**
 * @readonly
 * @enum {number}
 * @type {{START: number, ENDED: number, WAIT: number, PLAYING: number}}
 * @memberOf module:enums
 */
const gameStatus = {
    WAIT: 0,
    START: 1,
    PLAYING: 2,
    ENDED: 3
};

/**
 * @readonly
 * @enum {string}
 * @type {{EASY: string, MEDIUM: string, HARD: string}}
 * @memberOf module:enums
 */
const gameDifficulty = {
    EASY: 'EASY',
    MEDIUM: 'MEDIUM',
    HARD: 'HARD'
};

/**
 * @readonly
 * @enum {number}
 * @type {{MULTIPLAYER: number, SINGLEPLAYER: number}}
 * @memberOf module:enums
 */
const gameMode = {
    SINGLEPLAYER: 0,
    MULTIPLAYER: 1
};

/**
 * @readonly
 * @enum {number}
 * @type {{DRAW: number, P1WIN: number, P2WIN: number}}
 * @memberOf module:enums
 */
const gameResult = {
    DRAW: 0,
    P1WIN: 1,
    P2WIN: 2
};

/**
 * @readonly
 * @enum {string}
 * @type {{EASY: string, MEDIUM: string, HARD: string}}
 * @memberOf module:enums
 */
const coreDifficulty = {
    EASY: 'easy',
    MEDIUM: 'normal',
    HARD: 'hard'
};

/**
 * @readonly
 * @enum {string}
 * @type {{MULTIPLAYER: string, SINGLEPLAYER: string}}
 * @memberOf module:enums
 */
const coreMode = {
    SINGLEPLAYER: 'single-player',
    MULTIPLAYER: 'multi-player'
};

/**
 * @readonly
 * @enum {string}
 * @type {{DRAW: string, LOSE: string, WIN: string}}
 * @memberOf module:enums
 */
const coreResult = {
    DRAW: 'draw',
    WIN: 'win',
    LOSE: 'lose'
};

module.exports = {
    gameStatus,
    gameDifficulty,
    gameMode,
    gameResult,
    coreDifficulty,
    coreMode,
    coreResult
};