const recipeClass = require("../models/recipe.js");
const ingredientDB = require("../accessDB/ingredientDB");
const recipeDB = require("../accessDB/recipeDB");

/**
 * @module functions/playAux
 */
/**
 * <hr><h4><b>getRecipes</b>(difficultyMode, numberOfRecipes)</h4>
 * @async
 * @param {string} difficultyMode
 * @param {number} numberOfRecipes
 * @return {Promise<object[]>} recipes
 * @memberOf module:functions/playAux
 */
async function getRecipes(difficultyMode, numberOfRecipes) {
    let recipeArrayJson = await recipeDB.getByDifficulty(difficultyMode)
    let arrayOfRecipes = [];
    let i = 0;

    if (recipeArrayJson.length < numberOfRecipes)
        throw "Erro: Receitas na BD insuficientes";

    while (i < numberOfRecipes) {
        let item = recipeArrayJson.splice(Math.floor(Math.random() * recipeArrayJson.length), 1)[0];

        let recipeNew = Object.create(recipeClass)
        recipeNew.fromJson(item)
        arrayOfRecipes.push(recipeNew);
        i++;
    }
    return arrayOfRecipes;
}

/**
 * <hr><h4><b>getIngredients</b>(recipes, minIngredients, minRandoms)</h4>
 * @async
 * @param {object[]} recipes
 * @param {number} minIngredients
 * @param {number} minRandoms
 * @return {Promise<string[]>}
 * @memberOf module:functions/playAux
 */
async function getIngredients(recipes, minIngredients, minRandoms) {
    let allIngredients = [];

    recipes.forEach(recipe => {
        recipe.getIngredients().forEach(ingredient => {
            allIngredients.push(ingredient.getName());
        });
    });

    let recipesIngredients = Array.from(new Set(allIngredients));

    recipesIngredients.sort();


    let ingredientsArrayJson = await ingredientDB.getAll()

    let allDBIngredients = [];

    ingredientsArrayJson.forEach(ingredient => {
        allDBIngredients.push(ingredient.name);
    });

    let DBIngredients = allDBIngredients.filter(ing => !recipesIngredients.includes(ing));

    let n;

    if (recipesIngredients.length + minRandoms >= minIngredients)
        n = minRandoms;
    else
        n = minIngredients - recipesIngredients.length;


    let ingredients = [];

    if (n >= DBIngredients.length) {
        ingredients = DBIngredients;
    } else {
        let i = 0;
        while (i < n) {
            let item = DBIngredients.splice(Math.floor(Math.random() * DBIngredients.length), 1)[0];

            ingredients.push(item);
            i++;
        }
    }

    ingredients = ingredients.concat(recipesIngredients).sort();

    return ingredients;
}

module.exports = {
    getRecipes,
    getIngredients
};