const rankDB = require('../accessDB/rankDB');
const rankClass = require('../models/rank');
const playerDB = require('../accessDB/playerDB');
const { gameResult } = require("./enums");
const { gameMode } = require("./enums");

/**
 * @module functions/rank
 */
/**
 * <hr><h4><b>compareRanks</b>(a, b)</h4>
 * @param {object} a
 * @param {object} b
 * @return {number}
 * @memberOf module:functions/rank
 */
function compareRanks(a, b) {
    if (a.totalCoins < b.totalCoins) return -1;
    if (a.totalCoins > b.totalCoins) return 1;
    return 0;
}

/**
 * <hr><h4><b>getRank</b>(playerId)</h4>
 * @async
 * @param {string} playerId
 * @return {Promise<object[]>}
 * @memberOf module:functions/rank
 */
async function getRank(playerId) {
    let ranks = await rankDB.getAll();
    ranks.sort(compareRanks);
    ranks.reverse();

    let rank = ranks.find(item => {
        return item.id === playerId
    })

    let pos = ranks.indexOf(rank) + 1;

    let top5 = ranks.slice(0, 5);

    let topObj = [];
    let i = 1;
    top5.forEach(item => {
        let obj = Object.create(rankClass);
        obj.fromJson(item);
        obj.setPosition(i);
        topObj.push(obj);
        i += 1;
    })

    if (pos > 5) {
        let obj = Object.create(rankClass);
        obj.fromJson(rank);
        obj.setPosition(pos);
        topObj.push(obj);
    }

    let jsonArray = [];

    topObj.forEach(item => {
        if (item.id === playerId) {
            jsonArray.push(item.toJsonFront());
        } else {
            jsonArray.push(item.toJsonSimple());
        }
    })

    return jsonArray;
}

/**
 * <hr><h4><b>newRank</b>(playerId)</h4>
 * @param {string} playerId
 * @return {object}
 * @memberOf module:functions/rank
 */
function newRank(playerId) {
    return {
        id: playerId,
        playerName: '',
        totalCoins: 0,
        nGamesSingle: 0,
        nGamesMulti: 0,
        nMultiWin: 0,
        nMultiLoss: 0,
        totalRecipes: 0,
    }
}

/**
 * <hr><h4><b>getPlayerId</b>(gameObj, isP1)</h4>
 * @param {object} gameObj
 * @param {boolean} isP1
 * @return {string}
 * @memberOf module:functions/rank
 */
function getPlayerId(gameObj, isP1) {
    if (isP1) return gameObj.playerId1;
    else return gameObj.playerId2;
}

/**
 * <hr><h4><b>setRank</b>(gameObj, isP1)</h4>
 * @async
 * @param {object} gameObj
 * @param {boolean} isP1
 * @return {Promise<void>}
 * @memberOf module:functions/rank
 */
async function setRank(gameObj, isP1) {
    let oldRankJson, playerJson, rank;
    await rankDB.getOne(getPlayerId(gameObj, isP1))
        .then(data1 => {
            oldRankJson = data1;
            return playerDB.getOne(getPlayerId(gameObj, isP1))
        })
        .then(data2 => {
            playerJson = data2
            return setRankAux(gameObj, isP1, oldRankJson, playerJson)
        })
        .then(data3 => {
            rank = data3;
            return rank.save();
        });
}

/**
 * <hr><h4><b>setRankAux</b>(gameObj, isP1, oldRankJson, playerJson)</h4>
 * @async
 * @param {object} gameObj
 * @param {boolean} isP1
 * @param {object} oldRankJson
 * @param {object} playerJson
 * @return {Promise<object>}
 * @memberOf module:functions/rank
 */
async function setRankAux(gameObj, isP1, oldRankJson, playerJson) {
    let playerName = playerJson.name;

    if (!oldRankJson) oldRankJson = newRank(playerJson.id);

    let rank = Object.create(rankClass);
    rank.fromJson(oldRankJson);

    rank.playerName = playerName;
    if (gameObj.isP1)
        rank.totalCoins += gameObj.cookieCoinsP1;
    else
        rank.totalCoins += gameObj.cookieCoinsP2;
    if (gameObj.mode === gameMode.SINGLEPLAYER)
        rank.nGamesSingle += 1;
    else {
        rank.nGamesMulti += 1;
        switch (gameObj.result) {
            case gameResult.P1WIN: {
                    if (isP1) rank.nMultiWin += 1;
                    else rank.nMultiLoss += 1;
                    break
                }
            case gameResult.P2WIN: {
                    if (isP1) rank.nMultiLoss += 1;
                    else rank.nMultiWin += 1;
                    break
                }
        }
    }

    if (isP1){
        gameObj.recipesDoneP1.forEach(item => {
            rank.totalRecipes += item.number;
        });
    } else {
        gameObj.recipesDoneP2.forEach(item => {
            rank.totalRecipes += item.number;
        });
    }

    return rank
}

module.exports = {
    getRank,
    setRank
};