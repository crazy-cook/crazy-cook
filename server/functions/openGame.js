const playerClass = require('../models/player');
const configClass = require('../models/config');
const sessionDB = require('../accessDB/sessionDB');
const playerDB = require('../accessDB/playerDB');

/**
 * @module functions/openGame
 */
/**
 * @type {Map<string, object>}
 * @memberOf module:functions/openGame
 */
let sessions = new Map();
/**
 * @type {Map<string, object>}
 * @memberOf module:functions/openGame
 */
let configs = new Map();

/**
 * <hr><h4><b>openGame</b>(json)</h4>
 * @param {object} json
 * @memberOf module:functions/openGame
 */
function openGame(json) {
    let jsonPlayer = json.user;
    let player = Object.create(playerClass);
    let id = json['session-id']
    player.init(jsonPlayer.id, jsonPlayer.name, jsonPlayer.avatar, jsonPlayer.mail, jsonPlayer['birth_date']);
    sessions.set(id, player);
    let config = Object.create(configClass);
    config.getFromDB(jsonPlayer.id).then(r => {
        configs.set(id, config);
        config.save();
    });
    sessionDB.putOne({id: id, playerId: jsonPlayer.id});
    player.save();
}

/**
 * <hr><h4><b>getFromDB</b>(id)</h4>
 * @param {string} id
 * @memberOf module:functions/openGame
 */
function getFromDB(id) {
    sessionDB.getOne(id).then(session => {
        playerDB.getOne(session.playerId).then(json => {
            let newJson = {
                id: json.id,
                avatar: json.avatar,
                name: json.name,
                mail: json.email,
                birthDate: json.birthdate
            }
            openGame({'session-id': id, user: newJson})
        })
    })
}

module.exports = {
    openGame,
    getFromDB,
    sessions,
    configs
}