const configEnv = require('../environments');
const mongo = require('mongodb');
const dbUrl = configEnv.db.connection + '/';
const connectionParams = {
    //useNewUrlParser: true,
    //useCreateIndex: true,
    useUnifiedTopology: true
};

/**
 * @module connection
 */
/**
 * <hr><h4><b>save</b>(name, objId, obj)</h4><br>Salva um objeto na db (upsert)
 * @memberOf module:connection
 * @param {string} name
 * @param {(string|number|null)} objId
 * @param {object} obj
 */
function save(name, objId, obj) {
    mongo.MongoClient.connect(dbUrl, connectionParams, (error, result) => {
        if (error) throw error;
        let dbo = result.db(configEnv.db.name);
        if (objId !== null) {
            let json;
            if (name === 'session') json = obj
            else json = obj.toJson()
            dbo.collection(name + 's').updateOne({ id: objId }, { $set: json }, { upsert: true })
                .then(ful => {
                    console.log(name + ' upsert')
                    result.close();
                }, rej => {
                    console.log(name + ' not upsert')
                    result.close();
                });
        } else {
            dbo.collection(name + 's').insertOne(obj.toJson())
                .then(ful => {
                    console.log(name + ' insert')
                    result.close();
                }, rej => {
                    console.log(name + ' not insert')
                    result.close();
                });
        }
    });
}

/**
 * <hr><h4><b>search</b>(query, name)</h4><br>Encontra os item da db que correspondem a pesquisa
 * @async
 * @memberOf module:connection
 * @param {object} query - json
 * @param {string} name
 * @return {Promise<object[]>} JSONArray
 */
async function search(query, name) {
    return new Promise(function(resolve, reject) {
        mongo.MongoClient.connect(dbUrl, connectionParams, (error, result) => {
            if (error) throw error;
            let dbo = result.db(configEnv.db.name);
            let collectionName = name === 'difficulty' ? 'difficulties' : name + 's';
            dbo.collection(collectionName).find(query).toArray(function(err, res) {
                result.close();
                if (err) {
                    console.log(name + ' not find')
                    reject(err)
                } else {
                    console.log(name + ' find')
                    resolve(res);
                }
            });
        });
    });
}

module.exports = {
    save,
    search
}