module.exports = {
    configureWebpack: {
      devServer: {
        progress: false
      },
      module: {
        rules: [
          {
            test: /\.worker\.js$/,
            use: { loader: 'worker-loader' }
          }
        ]
      }
    }
}