import Vue from 'vue'
import Router from 'vue-router'

import Home from '../components/Home.vue'
import Profile from '../components/Profile.vue'
import Game from '../components/Game.vue'
import Rules from '../components/Rules.vue'
import Settings from '../components/Settings.vue'
import Loading from '../components/Loading.vue'
import PageNotFound from '../components/PageNotFound.vue'
import GamesStatistics from '../components/GamesStatistics.vue'
import RankingStatistics from '../components/RankingStatistics.vue'
import StatisticsStatistics from '../components/StatisticsStatistics.vue'
import GameStart from '../components/GameStart.vue'
import SinglePlayer from '../components/SinglePlayer.vue'
import WaitingRoom from '../components/WaitingRoom.vue'
import GameMultiplayer from '../components/GameMultiplayer.vue'
import EndGame from '../components/EndGame.vue'
import EndGameMultiplayer from '../components/EndGameMultiplayer.vue'

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/', name: 'Loading', component: Loading },
        { path: '/home', name: 'Home', component: Home },
        { path: '/profile', name: 'Profile', component: Profile },
        { path: '/rules', name: 'Rules', component: Rules },
        { path: '/game', name: 'Game', component: Game },
        { path: '*', name: 'Error', component: PageNotFound },
        { path: '/settings', name: 'Settings', component: Settings },
        { path: '/gamesStatistics', name: 'GamesStatistics', component: GamesStatistics },
        { path: '/rankingStatistics', name: 'ClassificacaoStatistics', component: RankingStatistics },
        { path: '/statisticsStatistics', name: 'StatisticsStatistics', component: StatisticsStatistics },
        { path: '/gameStart', name: 'GameStart', component: GameStart },
        { path: '/singlePlayer', name: 'SinglePlayer', component: SinglePlayer },
        { path: '/waitingRoom', name: 'WaitingRoom', component: WaitingRoom },
        { path: '/gameMultiplayer', name: 'GameMultiplayer', component: GameMultiplayer },
        { path: '/endGame', name: 'EndGame', component: EndGame },
        { path: '/endGameMultiplayer', name: 'EndGameMultiplayer', component: EndGameMultiplayer }
    ],
    mode: 'history'
})