if (process.env.NODE_ENV !== 'pro') {
    require('dotenv').config();
}

import Vue from 'vue'
import App from './App.vue'
import router from './router/router.js'

Vue.config.productionTip = false

let globalData = new Vue({
    data: { $theme: 0 }
});

Vue.mixin({
    computed: {
        $theme: {
            get: function() { return globalData.$data.$theme },
            set: function(newTheme) { globalData.$data.$theme = newTheme; }
        }
    }
})

new Vue({
    el: '#app',
    router,
    data: {
        bobby: {
            name: "bobby"
        }
    },
    render: h => h(App)
}).$mount('#app')