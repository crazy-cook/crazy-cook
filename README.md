# **Crazy-Cook**

<img src="./logo.png" alt="Crazy-Cook Logo" width= "300px">

## Running
```bash
# install dependencies
npm install

# run client & server
npm run start
```

## Equipa
- André Alves
- Daniel Tolledo
- Ema Barão
- João Pereira
- Pedro Freitas
- Ricardo Rocha

## Documentação
- [APIs](http://35.180.123.44:8888/docs/apis)
- [JSDoc](http://35.180.123.44:8888/docs/jsdoc)

### Projeto das UCs de PAI e QS do MES do IPS
Projeto das Unidades Curriculares de Programação Avançada para a Internet e Qualidade de Software do 1º Semestre de Mestrado de Engenharia de Software do Instituto Politécnico de Setubal.

***Fevereiro de 2021***