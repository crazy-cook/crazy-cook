let env_vars;
if (process.env.NODE_ENV.trim() === 'dev') {
    env_vars = {
        "db": {
            "connection": "mongodb+srv://" + process.env.ATLAS_USER + ":" + process.env.ATLAS_KEY + "@cluster-crazy-cook.vyyco.mongodb.net/crazy-cook-dev?retryWrites=true",
            "name": "crazy-cook-dev"
        },
        "host": 'localhost'
    }
} else {
    env_vars = {
        /*"db": {
            "connection": "mongodb+srv://" + process.env.ATLAS_USER + ":" + process.env.ATLAS_KEY + "@crazy-cook-pro.pelbg.mongodb.net/crazy-cook-pro?retryWrites=true",
            "name": "crazy-cook-pro"
        },*/
        "db": {
            "connection": "mongodb+srv://" + process.env.ATLAS_USER + ":" + process.env.ATLAS_KEY + "@cluster-crazy-cook.vyyco.mongodb.net/crazy-cook-dev?retryWrites=true",
            "name": "crazy-cook-dev"
        },
        "host": "35.180.123.44"
    }
}
module.exports = env_vars;